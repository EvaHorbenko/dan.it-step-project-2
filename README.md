Список использованных технологий:
-     Chrome DevTools
-     HTML5
-     SASS
-     BEM
-     Npm
-     GULP
-     Git version control system


Состав участников проекта:
-     EvaHorbenko: @EvaHorbenko
-     Yuriy Motsnyy: @y.motsnyy


Задачи выполняемые участниками:

    EvaHorbenko: 
        -   создиние удаленного репозитория
        -   header layout + js burger-menu + hover for header
        -   section "People Are Talking About Fork"
        -   + adaptive & responsive layout for this sections
        -   создание ссылки на GitLab Pages

    Yuriy Motsnyy:
        -   section "Revolutionary Editor"
        -   section "Here is what you get"
        -   section "Fork Subscription Pricing"
        -   + adaptive & responsive layout for this sections
    
