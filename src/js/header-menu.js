const showMenuBtn = document.querySelector('.header__show-menu-btn--js');
const menuForShow = document.querySelector('.header__menu--js');
const headerMenuItemCollection = document.querySelectorAll('.header__menu__item--js')

const toggleMenu = function () {
    menuForShow.classList.toggle('header__menu--hidden');
    if (menuForShow.classList.contains('header__menu--hidden')) {
        showMenuBtn.innerHTML = '<i class="fas fa-bars"></i>'
    } else {
        showMenuBtn.innerHTML = '<i class="fas fa-times"></i>'
    }
}

const activateItem = function (event) {
    for (let i = 0; i < headerMenuItemCollection.length; i++) {
        headerMenuItemCollection[i].classList.remove('header__menu__item--active');
    }
    let target = event.target.closest('.header__menu__item--js');
    if (!target.classList.contains('header__menu__item--active')) {
        target.classList.add('header__menu__item--active')
    }
}

showMenuBtn.addEventListener('click', toggleMenu);
menuForShow.addEventListener('click', activateItem);